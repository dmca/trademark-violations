# Trademark Violations

This repository stores all Trademark Violations reported by request. Please refer to our [Trademark Violations Policy](https://www.codelinaro.org/policies/trademark-policy.html) for further information.

~ CodeLinaro Support
